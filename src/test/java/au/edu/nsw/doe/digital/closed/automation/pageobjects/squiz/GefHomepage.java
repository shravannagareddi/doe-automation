package au.edu.nsw.doe.digital.closed.automation.pageobjects.squiz;

import org.openqa.selenium.By;

/**
 * Created by cpigden on 14/12/2015.
 */
public class GefHomepage {

    public static By heroBannerButton = By.xpath("//a[contains(.,'Hero banner button')]");
    public static By provideFeedbackButton = By.xpath("//button[contains(.,'Provide Feedback')]");
    public static By provideFeedbackDialogue = By.xpath("//h2[@class='dialog-title']");




}
