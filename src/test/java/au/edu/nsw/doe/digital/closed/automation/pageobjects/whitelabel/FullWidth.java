package au.edu.nsw.doe.digital.closed.automation.pageobjects.whitelabel;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 9/3/17.
 */
public class FullWidth {

    public static By contentBanner = By.id("banner_auto");
    public static By breadcrumbs = By.id("breadcrumbs_auto");
    public static By teasers = By.id("teasers_auto");
    public static By linkGroup = By.id("linkGroup_auto");
    public static By expandedNav = By.id("expandedNav_auto");
    public static By aZAnchors = By.id("azAnchors_auto");
    public static By aZList = By.id("azList_auto");
    public static By landingHeader = By.id("landingHeader_auto");
    public static By tableOfContents = By.id("tableOfContents_auto");
    public static By resultsTable = By.id("resultsTable_auto");
    public static By contentContainer = By.id("contentContainer_auto");
    public static By catalogue = By.id("catalogue_auto");
    public static By drawer = By.id("drawer_auto");
    public static By eduBanner = By.id("eduBanner_auto");
    public static By noticeRibbon = By.id("noticeRibbon_auto");

}
