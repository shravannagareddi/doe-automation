package au.edu.nsw.doe.digital.closed.automation.pageobjects.squiz;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 7/2/17.
 */
public class GefEdConnect {
    public static By edConnectButton = By.id("drawer84tab");
    public static By edconnectDrawer = By.xpath("//a[@aria-expanded='true']");
    public static By edConnect = By.id("drawer84");
}
