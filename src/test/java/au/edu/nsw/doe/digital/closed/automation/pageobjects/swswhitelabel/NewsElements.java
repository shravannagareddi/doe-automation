package au.edu.nsw.doe.digital.closed.automation.pageobjects.swswhitelabel;

import org.openqa.selenium.By;

public class NewsElements {

    //layouts
    public static By newsFullWidthFullImage = By.id("newsTemplate1");
    public static By newsFullWidth = By.id("newsTemplate2");
    public static By newsTwoColumn = By.id("newsTemplate3");
    public static By newsOneColumn = By.id("newsTemplate4");
}
