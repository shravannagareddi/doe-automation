package au.edu.nsw.doe.digital.closed.automation.pageobjects.swswhitelabel;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 11/8/17.
 */
public class ContentElements {

    //Clickable web elements
    public static By showHide = By.id("at443");
    public static By showHideComponent = By.cssSelector(".uk-active");
    public static By tab = By.id("label_tabs294");
    public static By tabComponent = By.cssSelector(".gef-tabs--active :nth-child(1)");
}


