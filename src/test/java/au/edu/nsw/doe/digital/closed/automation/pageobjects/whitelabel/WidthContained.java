package au.edu.nsw.doe.digital.closed.automation.pageobjects.whitelabel;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 10/3/17.
 */
public class WidthContained {

    //Main column
    public static By leadParagraph = By.id("leadparagraph_auto");
    public static By showHide = By.id("showhide_auto");
    public static By tabs = By.id("tabs_auto");
    public static By callOutBox = By.id("calloutbox_auto");
    public static By metaData = By.id("metadata_auto");
    public static By breadcrumbs = By.id("breadcrumbs_auto");
    public static By datePicker = By.id("datepicker_auto");
    public static By downLoad = By.id("downloadbox_auto");
    public static By featuredTeaser = By.id("featuredteaser_auto");
    public static By categorySelect = By.id("categoryselect_auto");
    public static By newsTeasers = By.id("newsteasers_auto");
    public static By category = By.id("category_auto");
    public static By tags = By.id("tags_auto");
    public static By shareThis = By.id("sharethis_auto");
    public static By searchResults = By.id("searchresults_auto");
    public static By pagination = By.id("pagination_auto");
    public static By anchorBox = By.id("anchor-box_auto");
    public static By searchBox = By.id("searchbox_auto");
    public static By secureIndicator = By.id("secure-indicator_auto");
    public static By featuredTeaserVariant = By.id("featuredteaservariant_auto");

    //Right hand column
    public static By sideNavigation = By.id("sidenavigation_auto");
    public static By categoryList = By.id("categorylist_auto");
    public static By contentPanel = By.id("contentpanel_auto");


}
