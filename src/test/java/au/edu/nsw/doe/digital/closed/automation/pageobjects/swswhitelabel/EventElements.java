package au.edu.nsw.doe.digital.closed.automation.pageobjects.swswhitelabel;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 11/8/17.
 */
public class EventElements {

    //Clickable web elements
    public static By eventFull = By.id("eventfull");
    public static By eventSixtySix = By.id("eventsixtysix");
    public static By eventThirtyThree = By.id("eventthirtythree");

}


