package au.edu.nsw.doe.digital.closed.automation.pageobjects.whitelabel;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 1/3/17.
 */
public class DefaultElements {
    public static By linkList = By.id("links_auto");
    public static By typography = By.id("typography_auto");
    public static By blockQuote = By.id("blockquote_auto");
    public static By iFrame = By.id("iframe_auto");
    public static By lists = By.id("lists_auto");
    public static By imageCaption = By.id("imagecaption_auto");
    public static By buttonSubmit = By.id("buttonsubmit_auto");
    public static By table = By.id("table_auto");
    public static By captionmatrix = By.id("imagecaption_matrix");
}
