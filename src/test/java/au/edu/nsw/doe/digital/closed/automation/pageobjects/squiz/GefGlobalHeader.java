package au.edu.nsw.doe.digital.closed.automation.pageobjects.squiz;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 6/2/17.
 */
public class GefGlobalHeader {
    public static By searchField = By.xpath("//input[@id='search']");
    public static By searchButton = By.id("btnSearch17");

}
