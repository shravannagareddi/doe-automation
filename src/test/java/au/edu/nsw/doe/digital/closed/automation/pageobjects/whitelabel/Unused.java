package au.edu.nsw.doe.digital.closed.automation.pageobjects.whitelabel;

import org.openqa.selenium.By;

/**
 * Created by christopherpigden on 10/3/17.
 */
public class Unused {

    public static By maintenanceRibbon = By.id("maintenanceRibbon_auto");
    public static By iconAndLabel = By.id("iconAndLabel_auto");
    public static By primaryHub = By.id("examplePrimaryHub_auto");
    public static By expandedNav = By.id("expandedNav_auto");


    //Right hand column
    public static By newsLetterSignUp = By.id("newsletterSignup_auto");
    public static By radioList = By.id("radioList_auto");
    public static By loginLink = By.id("loginLink_auto");

}
