package au.edu.nsw.doe.digital.closed.automation.pageobjects.squiz;

import org.openqa.selenium.By;

/**
 * Created by cpigden on 2/12/2015.
 */
public class GefShowHide {

    public static By showHide1 = (By.xpath("//h3[@id='at136164']"));
    public static By showHide2 = (By.xpath("//h3[@id='at136168']"));
    public static By showHide3 = (By.xpath("//h3[@id='at136172']"));
    public static By showHideContent1 = (By.xpath("//div[@class='uk-accordion-content uk-active']"));

}
