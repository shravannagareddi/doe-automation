package au.edu.nsw.doe.digital.closed.automation.pageobjects.swswhitelabel;

import org.openqa.selenium.By;

public class ImageElements {

    public static By image = By.xpath("//div[contains(@id, 'albumColumn100')]//div[contains(@class, 'sws-component-self-contain__first')]");
}
